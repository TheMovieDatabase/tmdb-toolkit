// swift-tools-version:5.1
import PackageDescription

let package = Package(
    name: "TMDBToolkit",
    platforms: [.iOS(.v13), .tvOS(.v12), .macOS(.v10_15)],
    products: [
        .library(
            name: "TMDBToolkit",
            targets: ["TMDBToolkit"]
        )
    ],
    dependencies: [
        .package(url: "https://gitlab.com/guacalabs/swift/networking-toolkit.git", .branch("master"))
    ],
    targets: [
        .target(
            name: "TMDBToolkit",
            dependencies: ["NetworkingToolkit"]
        )
    ],
    swiftLanguageVersions: [.v5]
)
