import Foundation
import Combine

public final class TMDBService {
    let apiClient = ApiClient()

    public init() {}

    private var disposables: Set<AnyCancellable> = []
}

public extension TMDBService {
    func moviesList(_ path: String) -> Future<MoviesList, Error> {
        return Future { promise in
            self.apiClient.get(endpoint: .list(path), responseType: MoviesList.self)
                .sink(receiveCompletion: { (completion) in
                    if case .failure(let error) = completion {
                        promise(.failure(error))
                    }
                }, receiveValue: { moviesList in
                    promise(.success(moviesList))
                })
                .store(in: &self.disposables)
        }
    }
}
