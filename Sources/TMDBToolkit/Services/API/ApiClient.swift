import Foundation
import Combine
import NetworkingToolkit

final class ApiClient {
    private let host: String
    private let session: URLSession
    private let dispatchQueue: DispatchQueue
    private let decoder: JSONDecoder

    init(
        host: String = Constants.apiHost,
        session: URLSession = .shared,
        dispatchQueue: DispatchQueue = .global(),
        decoder: JSONDecoder = .default
    ) {
        self.host = host
        self.session = session
        self.dispatchQueue = dispatchQueue
        self.decoder = decoder
    }
}

extension ApiClient {
    func get<T: Decodable>(endpoint: Endpoint, responseType: T.Type) -> AnyPublisher<T, Error> {
        do {
            let url = try URL.Builder()
                .setHost(host)
                .setPath(endpoint.path)
                .setQuery(endpoint.query)
                .build()

            return session
                .dataTaskPublisher(for: url)
                .subscribe(on: dispatchQueue)
                .map(\.data)
                .decode(type: responseType, decoder: decoder)
                .receive(on: DispatchQueue.main)
                .eraseToAnyPublisher()
        } catch {
            return Fail<T, Error>(error: error)
                .eraseToAnyPublisher()
        }
    }
}
