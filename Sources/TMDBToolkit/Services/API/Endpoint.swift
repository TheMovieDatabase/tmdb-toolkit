import Foundation

struct Endpoint {
    let path: String
    let query: [URLQueryItem]?
}

extension Endpoint {
    static func list(_ path: String) -> Endpoint {
        Endpoint(
            path: path,
            query: [.apiKey(), .language()]
        )
    }
}
