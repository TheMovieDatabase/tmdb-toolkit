import Foundation

extension URLQueryItem {
    static func apiKey(_ apiKey: String = Constants.apiKey) -> URLQueryItem {
        URLQueryItem(name: "api_key", value: apiKey)
    }

    static func language(id: String = Locale.current.identifier) -> URLQueryItem {
        URLQueryItem(name: "language", value: id)
    }
}
