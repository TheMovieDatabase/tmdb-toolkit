import Foundation

extension JSONDecoder {
    static let `default`: JSONDecoder = {
        let decoder = JSONDecoder()

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"

        decoder.dateDecodingStrategy = .formatted(dateFormatter)

        return decoder
    }()
}
