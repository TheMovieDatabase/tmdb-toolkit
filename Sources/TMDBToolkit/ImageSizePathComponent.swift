import Foundation

public enum ImageSizePathComponent {
    case width(Int)
    case original

    var path: String {
        switch self {
        case .width(let width):
            return "/w\(width)"
        case .original: return
            "/original"
        }
    }
}
