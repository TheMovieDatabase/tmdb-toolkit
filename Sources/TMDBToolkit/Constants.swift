import Foundation
import NetworkingToolkit

enum Constants {
    static let apiKey = "b2ba669b4bbc3fec8ccd24924c8dccc4"
    static let apiHost = "api.themoviedb.org"
    static let imagesBaseURL: URL = "https://image.tmdb.org/t/p"
}
