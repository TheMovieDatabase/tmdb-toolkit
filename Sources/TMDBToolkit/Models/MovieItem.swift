import Foundation

public struct MovieItem {
    public let id: Int
    public let title: String
    public let originalTitle: String
    public let originalLanguage: String
    public let posterPath: String?
    public let backdropPath: String?
    public let overview: String
    public let popularity: Double
    public let voteCount: Int
    public let voteAverage: Double
    public let genreIDs: [Int]
    public let isAdultContent: Bool
    public let releaseDate: Date
}

public extension MovieItem {
    func posterURL(with sizeComponent: ImageSizePathComponent) -> URL? {
        posterPath
            .flatMap {
                Constants
                    .imagesBaseURL
                    .appendingPathComponent(sizeComponent.path)
                    .appendingPathComponent($0)
            }
    }
}

extension MovieItem: Decodable {
    private enum CodingKeys: String, CodingKey {
        case id, title, overview, popularity
        case originalTitle = "original_title"
        case originalLanguage = "original_language"
        case posterPath = "poster_path"
        case backdropPath = "backdrop_path"
        case voteCount = "vote_count"
        case voteAverage = "vote_average"
        case genreIDs = "genre_ids"
        case isAdultContent = "adult"
        case releaseDate = "release_date"
    }
}

extension MovieItem: Identifiable {}

extension MovieItem: Hashable {
    public func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension MovieItem: Equatable {
    public static func == (lhs: MovieItem, rhs: MovieItem) -> Bool {
        lhs.id == rhs.id
    }
}
