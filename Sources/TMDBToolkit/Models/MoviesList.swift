import Foundation

public struct MoviesList {
    public let page: Int
    public let totalPages: Int
    public let totalResults: Int
    public let results: [MovieItem]
}

extension MoviesList: Decodable {
    private enum CodingKeys: String, CodingKey {
        case page, results
        case totalPages = "total_pages"
        case totalResults = "total_results"
    }
}
